//Librerías
const express = require('express')
const app = express()
const http = require('http').Server(app)
const moment =require ('moment')
const jwt = require('jsonwebtoken')
const parser = require('body-parser')
const db = require('mongoose')
const morgan = require('morgan')
const cors = require('cors')

//Archivo de configuración
const config = require('./config')

//Conexión Base de Datos
db.Promise = global.Promise
db.connect(config.database + config.database_name, {useNewUrlParser: true}, function(error) {
  if(error) {
    console.log('Error MongoDB:', error);
  }
});
let MongoDB = db.connection
let ObjectId = require('mongodb').ObjectID

//Configuración del servidor Express
app.use(parser())
app.use(cors())
app.use(morgan(':date[iso] --> [:remote-addr] :method :url :status :response-time ms - :res[content-length]'))
app.set('key', config.secret)//secret es la llave de validaciòn con la que se encripta en JWT

let api = express.Router()

api.use((req, res, next) => {
  try {
    let token = req.headers.authorization.split(' ')[1]
    //let token = req.body.token || req.query.token || req.headers['x-access-token'];
    jwt.verify(token, app.get('key'), function(error, decoded) {
      if (error) {
        return res.json({ msg: 'error', data: 'Authorization_error' })
      }else{
        req.token = decoded
        next()
      }
    })
  }catch(e) {
    return res.status(403).json({msg: 'error', data: 'Authorization_required' })
  }
})

app.use('/api', api)

app.post('/login', (req, res) => {
  let user = req.body.user ? req.body.user.trim() : ''//Recibe valores, trim es borrar todos los espacios al final
  let pass = req.body.pass ? req.body.pass.trim() : ''

  let m = moment().hour(0).minute(0).second(0).millisecond(0)
  m = m.add(1, 'days')
  let ms = moment(m).diff(moment())
  let r = moment.duration(ms)
  let restante = Math.floor(r / 1000)

  MongoDB.collection('users').findOne({'user': user, 'pass': pass}, (error, find) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else if(find){
      res.json({msg: 'ok', token: jwt.sign({user: find._id, role: find.role}, app.get('key'), { expiresIn: restante })})
    }else{
      res.json({msg: 'error', data: 'not_found'})
    }
  })
})
// ##################################################################### TODO PARA EL USUARIO ☼
//PAPI USER
//Listar usuarios
api.get('/user', (req, res) => {
  console.log(req.token.role)
  switch(req.token.role){
    case 1: 
      MongoDB.collection('users').find({}).sort({user: 1}).toArray((error, find) => {
        if(error) {
          res.json({msg: 'error', error: error})
        }else if(find.length > 0) {
          res.json({msg: 'ok', result: find})
        }else{
          res.json({msg: 'ok', result: []})
        }
      })      
      break;
    default:
      res.json({msg: 'error',data: "no Authorization"})
      break;
  }
})

//Listar un usuario
api.get('/user/:id', (req, res) => {
  console.log(req.token.role)
  const id=req.params.id
  switch(req.token.role){
    case 1: 
      MongoDB.collection('users').findOne({'_id': db.Types.ObjectId(id)},(error, find) => { 
        if(error) {
          res.json({msg: 'error', error: error})
        }else if(find) {
          res.json({msg: 'ok', result: find})
        }else{
          res.json({msg: 'ok', result: []})
        }
      })      
      break;
    default:
      res.json({msg: 'error',data: "no Authorization"})
      break;
  }
})

//Crear usuario
api.post('/user', (req, res) => {
  // const user=req.token.user
  let data = req.body
  let json = {
    user: data.user ? data.user.trim() : '',
    pass: data.pass ? data.pass.trim() : '',//md5
    role: 2
  }
  MongoDB.collection('users').insertOne(json, (error, inserted) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', id: inserted.insertedId})
    }
  })
})

//Editar usuarios
api.put('/user/:id', (req, res) => {
  const id = req.params.id
  let data = req.body
  
  switch(req.token.role){
    case 1:
      let json = data.role ?{
        // user: data.user ? data.user.trim() : '',
        pass: data.pass ? data.pass.trim() : '',
        role: data.role ? data.role : 2,
      } : {
        pass: data.pass ? data.pass.trim() : '',        
      }    
      MongoDB.collection('users').update({'_id': db.Types.ObjectId(id)}, { $set: json} , (error, updated) => {
        if(error) {
          res.json({msg: 'error', data: 'Internal_error'})
        }else{
          res.json({msg: 'ok', id: id})
        }
      })
      break;
    case 2:
      if (id == req.token.user){
        let json = {
          pass: data.pass ? data.pass.trim() : '',
        }       
        MongoDB.collection('users').update({'_id': db.Types.ObjectId(id)}, { $set: json} , (error, updated) => {
          if(error) {
            res.json({msg: 'error', data: 'Internal_error'})
          }else{
            res.json({msg: 'ok', id: id})
          }
        })
      }
      else{
        res.json({msg:'error', data: 'no Authorization'})
      }
      break;
    default:
        res.json({msg:'error', data: 'no Authorization'})
      break;
  }

})

//Eliminar ususarios
api.delete('/user/:id', (req, res) => {
  const id = req.params.id
  switch(req.token.role){
    case 1:
      MongoDB.collection('users').remove({ '_id' : db.Types.ObjectId(id)}, (error, removed) => {
        if(error) {
          res.json({msg: 'error', data: 'Internal_error'})
        }else{
          res.json({msg: 'ok', id: id})
        }
      })
      break;
    default:
      res.json({msg:'error', data: 'no Authorization'})
      break;
  }
})
//############################################################################################## TODO PARA EL DICCIONARIO 
//API DICCIONARIO
//Listar
api.get('/', (req, res) => {
  console.log(req.token.user)
  MongoDB.collection('diccionario').find({}).sort({palabra: 1}).toArray((error, find) => {
    if(error) {
      res.json({msg: 'error', error: error})
    }else if(find.length > 0) {
      res.json({msg: 'ok', result: find})
    }else{
      res.json({msg: 'ok', result: []})
    }
  })
})

//Crear
api.post('/', (req, res) => {
  const user=req.token.user
  let data = req.body
  let json = {
    palabra: data.palabra ? data.palabra.trim() : '',
    significado: data.significado ? data.significado.trim() : '',
    tipo: data.tipo ? data.tipo.trim() : '',
    date: moment().format('DD/MM/YYYY hh:mm A'),
    usuario: user,
  }

  MongoDB.collection('diccionario').insertOne(json, (error, inserted) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', id: inserted.insertedId})
    }
  })
})

//Actualizar
api.put('/:id', (req, res) => {
  const id = req.params.id

  let data = req.body
  let json = {
    palabra: data.palabra ? data.palabra.trim() : '',
    significado: data.significado ? data.significado.trim() : '',
    tipo: data.tipo ? data.tipo.trim() : '',
    usuario: data.usuario ? data.usuario.trim() : '',
  }

  MongoDB.collection('diccionario').update({'_id': db.Types.ObjectId(id)}, json, (error, updated) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', id: id})
    }
  })
})

//Eliminar
api.delete('/:id', (req, res) => {
  const id = req.params.id

  MongoDB.collection('diccionario').remove({ '_id' : db.Types.ObjectId(id)}, (error, removed) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', id: id})
    }
  })
})
//Listar
api.get('/', (req, res) => {
  console.log(req.token.user)
  MongoDB.collection('diccionario').find({}).sort({palabra: 1}).toArray((error, find) => {
    if(error) {
      res.json({msg: 'error', error: error})
    }else if(find.length > 0) {
      //$lookup
      res.json({msg: 'ok', result: find})//
    }else{
      res.json({msg: 'ok', result: []})
    }
  })
})

//Crear
api.post('/', (req, res) => {
  const user=req.token.user
  let data = req.body
  let json = {
    palabra: data.palabra ? data.palabra.trim() : '',
    significado: data.significado ? data.significado.trim() : '',
    tipo: data.tipo ? data.tipo.trim() : '',
    usuario: user,
  }

  MongoDB.collection('diccionario').insertOne(json, (error, inserted) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', id: inserted.insertedId})
    }
  })
})

//Actualizar
api.put('/:id', (req, res) => {
  const id = req.params.id

  let data = req.body
  let json = {
    palabra: data.palabra ? data.palabra.trim() : '',
    significado: data.significado ? data.significado.trim() : '',
    tipo: data.tipo ? data.tipo.trim() : '',
    usuario: data.usuario ? data.usuario.trim() : '',
  }

  MongoDB.collection('diccionario').update({'_id': db.Types.ObjectId(id)}, json, (error, updated) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', id: id})
    }
  })
})

//Eliminar
api.delete('/:id', (req, res) => {
  const id = req.params.id

  MongoDB.collection('diccionario').remove({ '_id' : db.Types.ObjectId(id)}, (error, removed) => {
    if(error) {
      res.json({msg: 'error', data: 'Internal_error'})
    }else{
      res.json({msg: 'ok', id: id})
    }
  })
})

//404
app.get('/*', (req, res) => {
  res.json({
    msg: 'error',
    data: '404'
  })
})

//Server HTTP (Puerto 80 por defecto)
http.listen(config.port, function() {
  console.log('Servidor montado *:' + config.port)
})
